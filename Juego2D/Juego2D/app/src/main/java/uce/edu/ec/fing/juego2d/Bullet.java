package uce.edu.ec.fing.juego2d;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

//import static uce.edu.ec.fing.juego2d.GameView.screenRatioX;
//import static uce.edu.ec.fing.juego2d.GameView.screenRatioY;

public class Bullet {
    int x, y, width, height;
    Bitmap bala;

    Bullet(Resources res){
        bala = BitmapFactory.decodeResource(res, R.drawable.bullet);

        width = bala.getWidth();
        height = bala.getHeight();

        width /= 4;
        height /= 4;

       // width = (int) (width * screenRatioX);
        //height = (int) (height * screenRatioY);

        //System.out.println("sr bullet" + screenRatioX + " y "+screenRatioY);

        bala = Bitmap.createScaledBitmap(bala, width, height, false);
    }

    Rect getCollisionShape () {
        return new Rect(x, y,  x + width, y + height);
    }


}
