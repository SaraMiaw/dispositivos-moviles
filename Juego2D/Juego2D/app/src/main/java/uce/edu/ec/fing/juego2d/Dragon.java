package uce.edu.ec.fing.juego2d;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

//import static uce.edu.ec.fing.juego2d.GameView.screenRatioX;
//import static uce.edu.ec.fing.juego2d.GameView.screenRatioY;

public class Dragon {

    int toShoot = 0;
    boolean isGoingUp = false;
    int numRepeticionFrames = 3;
    int x, y, width, height, wingCounter = 1, shootCounter = 1;
    Bitmap shoot1, shoot2, shoot3, shoot4, dead;
    Bitmap dragon1, dragon2, dragon3, dragon4;
    private GameView gameView;


    public Dragon(GameView gameView, int screenY, Resources res){
        this.gameView = gameView;

        dragon1 = BitmapFactory.decodeResource(res, R.drawable.dragon1);
        dragon2 = BitmapFactory.decodeResource(res, R.drawable.dragon2);
        dragon3 = BitmapFactory.decodeResource(res, R.drawable.dragon3);
        dragon4 = BitmapFactory.decodeResource(res, R.drawable.dragon4);

        width = dragon1.getWidth();
        height = dragon1.getHeight();

        width /= 4;
        height /= 4;

        //width = (int) (width * screenRatioX);
        //height = (int) (height * screenRatioY);

        dragon1 = Bitmap.createScaledBitmap(dragon1, width, height, false);
        dragon2 = Bitmap.createScaledBitmap(dragon2, width, height, false);
        dragon3 = Bitmap.createScaledBitmap(dragon3, width, height, false);
        dragon4 = Bitmap.createScaledBitmap(dragon4, width, height, false);

        shoot1 = BitmapFactory.decodeResource(res, R.drawable.dshoot1);
        shoot2 = BitmapFactory.decodeResource(res, R.drawable.dshoot2);
        shoot3 = BitmapFactory.decodeResource(res, R.drawable.dshoot3);
        shoot4 = BitmapFactory.decodeResource(res, R.drawable.dshoot4);

        shoot1 = Bitmap.createScaledBitmap(shoot1, width, height, false);
        shoot2 = Bitmap.createScaledBitmap(shoot2, width, height, false);
        shoot3 = Bitmap.createScaledBitmap(shoot3, width, height, false);
        shoot4 = Bitmap.createScaledBitmap(shoot4, width, height, false);

        dead = BitmapFactory.decodeResource(res, R.drawable.dragon4);
        dead = Bitmap.createScaledBitmap(dead, width, height, false);

        y = screenY / 2;

        //x = (int) (64 * screenRatioX);
        x=64;
    }

    Bitmap getDragonImage(){
        if (toShoot != 0) {

            if (shootCounter == 1) {
                shootCounter++;
                return shoot1;
            }

            if (shootCounter == 2) {
                shootCounter++;
                return shoot2;
            }

            if (shootCounter == 3) {
                shootCounter++;
                return shoot3;
            }

            shootCounter = 1;
            toShoot--;
            gameView.newBullet();

            return shoot4;
        }

        if (wingCounter <= numRepeticionFrames){
            wingCounter++;
            return dragon1;
        }else
        if (wingCounter <= 2*numRepeticionFrames){
            wingCounter++;
            return dragon2;
        }else
        if (wingCounter <= 3*numRepeticionFrames){
            wingCounter++;
            return dragon3;
        }else
        if (wingCounter > 4*numRepeticionFrames){
            wingCounter=1;
        }
        wingCounter++;
        return dragon4;
    }

    Bitmap getDead () {

        return dead;
    }

    Collider getCollider(){
        return  new Collider(height * 0.4f, x + width*0.5f, y + height*0.5f);
    }
}
