package uce.edu.ec.fing.juego2d;

public class Collider {
    float radio;
    float x, y;

    public Collider(float radio, float x, float y) {
        this.radio = radio;
        this.x = x;
        this.y = y;
    }

    public static boolean existeColision(Collider a, Collider b){
        float distancia = (float) Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
        return distancia < (a.radio + b.radio);
    }
}
