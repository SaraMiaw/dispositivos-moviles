 package uce.edu.ec.fing.juego2d;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.view.MotionEvent;
import android.view.SurfaceView;

import androidx.annotation.RequiresApi;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameView extends SurfaceView implements  Runnable {

    private Thread hilo;
    private boolean  jugando, isGameOver = false;
    private int score = 0;
    public static int screenX, screenY;
    private static float screenRatioX, screenRatioY;
    private Paint paint;
    private List<Monstruo> monstruos;
    private SharedPreferences prefs;
    private Random random;
    private SoundPool soundPool;
    private List<Bullet> bullets;
    private int sound;
    private Dragon dragon;
    private GameActivity activity;
    private Background background1, background2;
    float dt;
    boolean addNuevoEnemigo = true;

    public GameView(GameActivity activity, int screenX, int screenY) {
        super(activity);

        this.activity = activity;

        prefs = activity.getSharedPreferences("game",Context.MODE_PRIVATE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .build();

            soundPool = new SoundPool.Builder()
                    .setAudioAttributes(audioAttributes)
                    .build();
        }else{
            soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        }
        sound = soundPool.load(activity, R.raw.shoot, 1);

        GameView.screenX = screenX;
        GameView.screenY = screenY;
        screenRatioX = screenX/1920f;
        screenRatioY = screenY/1080f;

        System.out.println("screen ratio inicializados "+screenRatioX+" y "+screenRatioY);

        background1 = new Background(screenX, screenY, getResources());
        background2 = new Background(screenX, screenY, getResources());

        dragon = new Dragon(this, screenY, getResources());
        bullets = new ArrayList<>();

        background2.x = screenX;

        paint = new Paint();
        paint.setTextSize(126);
        paint.setColor(Color.WHITE);

        monstruos = new ArrayList<>();

        for (int i = 0; i < 4; i++){
            Monstruo monstruo = new Monstruo(getResources());
            monstruos.add(monstruo);
        }

        random = new Random();

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void run() {
       long timeSinceStart;
        long oldTimeSinceStart = 0;
        long deltaTime = 1;
        while(jugando){
            timeSinceStart = System.nanoTime() / 1000;
            update();
            draw();
            sleep();
            deltaTime = timeSinceStart - oldTimeSinceStart;
            oldTimeSinceStart = timeSinceStart;
            dt = deltaTime/1000000f;
            System.out.println("delta dimte seg "+dt);
            System.out.println("delta timee "+deltaTime);
            long fps = (1000000/deltaTime);
            System.out.println("FPS "+fps);
        }
    }

    private void update(){
        if (score != 0 && score%10 == 0 && addNuevoEnemigo){
            Monstruo monstruo = new Monstruo(getResources());
            monstruo.x = -500;
            monstruos.add(monstruo);
            addNuevoEnemigo = false;
        }

        background1.x -= 20 * screenRatioX;
        background2.x -= 20 * screenRatioX;

//        background1.x -= ((float)dt / 3f) * screenRatioX;
//        background2.x -= ((float)dt / 3f) * screenRatioX;

        if (background1.x + background1.background.getWidth() < 0){
            background1.x = screenX;
        }

        if (background2.x + background2.background.getWidth() < 0){
            background2.x = screenX;
        }

        if (dragon.isGoingUp)
            dragon.y -= 30 * screenRatioY;
        else
            dragon.y += 30 * screenRatioY;

        if (dragon.y < 0)
            dragon.y = 0;

        if (dragon.y >= screenY - dragon.height)
            dragon.y = screenY - dragon.height;

        List<Bullet> trash = new ArrayList<>();
        for (Bullet bullet : bullets){
            bullet.x += 50 *screenRatioX;
            if (bullet.x > screenX){
                trash.add(bullet);
            }
            for (Monstruo monstruo: monstruos){
                if (Rect.intersects(monstruo.getCollisionShape(),
                        bullet.getCollisionShape())) {

                    score++;
                    monstruo.x = -500;
                    bullet.x = screenX + 500;
                    monstruo.wasShot = true;
                    if (!addNuevoEnemigo){
                        addNuevoEnemigo = true;
                    }
                }
            }
        }

        for (Bullet bullet : trash)
            bullets.remove(bullet);

        for (Monstruo monstruo : monstruos){
            monstruo.x -= monstruo.speed;

            if (monstruo.x + monstruo.width < 0){
                //Game over si no dispara un pajaro
//                if (!bird.wasShot) {
//                    isGameOver = true;
//                    return;
//                }

                int bound = (int) (30 * screenRatioX);
                monstruo.speed = random.nextInt(bound);

                if (monstruo.speed < 10 * screenRatioX)
                    monstruo.speed = (int) (10 * screenRatioX);

                monstruo.x = screenX;
                monstruo.y = random.nextInt(screenY - monstruo.height);

                monstruo.wasShot = false;
            }
            //if (Rect.intersects(monstruo.getCollisionShape(), dragon.getCollisionShape())) {
            if (Collider.existeColision(monstruo.getCollider(), dragon.getCollider())){
                isGameOver = true;
                return;
            }
        }
    }

    private void draw(){
        if (getHolder().getSurface().isValid()){
            Canvas canvas = getHolder().lockCanvas();
            canvas.drawBitmap(background1.background, background1.x, background1.y, paint);
            canvas.drawBitmap(background2.background, background2.x, background2.y, paint);


            for (Monstruo m : monstruos){
                canvas.drawBitmap(m.getMonstruoImage(), m.x, m.y, paint);
                //canvas.drawCircle(m.getCollider().x, m.getCollider().y, m.getCollider().radio, paint); //dibujar collider monstruo
            }

            canvas.drawText(score + "", screenX / 2f, 164, paint);

            if (isGameOver){
                jugando = false;
                canvas.drawBitmap(dragon.getDead(), dragon.x, dragon.y, paint);
                getHolder().unlockCanvasAndPost(canvas);
                saveIfHighScore();
                waitBeforeExiting ();
                return;
            }

            canvas.drawBitmap(dragon.getDragonImage(), dragon.x, dragon.y, paint);
           // canvas.drawCircle(dragon.getCollider().x, dragon.getCollider().y, dragon.getCollider().radio, new Paint()); // dibujar collider dragon

            for (Bullet bullet : bullets){
                canvas.drawBitmap(bullet.bala, bullet.x, bullet.y, paint);
            }
            getHolder().unlockCanvasAndPost(canvas);
        }
    }

    private void waitBeforeExiting() {
        try {
            Thread.sleep(3000);
            activity.startActivity(new Intent(activity, MainActivity.class));
            activity.finish();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void saveIfHighScore() {
        if (prefs.getInt("highscore", 0)<score){
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("highscore", score);
            editor.apply();
        }
    }

    private void sleep(){
        try {
            Thread.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void resume(){
        jugando = true;
        hilo = new Thread(this);
        hilo.start();
    }

    public void pause(){
        try{
            jugando=false;
            hilo.join();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                if (event.getX() < screenX/2f){
                    dragon.isGoingUp = true;
                }
                break;
            case MotionEvent.ACTION_UP:
                dragon.isGoingUp = false;
                if (event.getX() > screenX / 2f)
                    dragon.toShoot++;
                break;
        }
        return true;
    }

    public void newBullet() {
        if (!prefs.getBoolean("isMute", false))
            soundPool.play(sound, 1, 1, 0, 0, 1);

        Bullet bullet = new Bullet(getResources());
        bullet.x = dragon.x + dragon.width;
        bullet.y = dragon.y;
        bullets.add(bullet);
    }
}
